XEA
===

Event and Activity Management system.


Usage
=====

This repository contains both the core API Django REST Framework-based
project and the frontend React web app.

Django REST Framework subproject
--------------------------------

The DRF subproject is available at xea-core and has a `manage.py` file
as any Django application. Please refer to its own README file for
further instructions.


React subproject
----------------

~To be filled~


Usage with Docker
=================

~ To be filled (but it's probably a good idea to have a
docker-compose.yml at some point) ~
